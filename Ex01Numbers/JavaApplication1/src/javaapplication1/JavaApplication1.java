/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;
import java.util.*;
/**
 *
 * @author Lun
 */
public class JavaApplication1 {

    public static void main(String[] args) {
        Scanner myInput= new Scanner(System.in);
        
        double num1, num2, add, sub, mul, div;
        int opt;
        
        System.out.print("Enter the first number: ");
        num1= myInput.nextDouble();
        System.out.print("Enter the second number: ");
        num2= myInput.nextDouble();
        
        while(true){
            System.out.println("Enter 1 for Add");
            System.out.println("Enter 2 for Subtract");
            System.out.println("Enter 3 for Multiply");
            System.out.println("Enter 4 for Divide");
            System.out.println("Enter 0 for exit");
            opt= myInput.nextInt();
            
            if(opt < 0 || opt > 4){
                System.out.println("Enter the valid choice.");
            }
            
            switch(opt){
                case 1:
                    add= num1 + num2;
                    System.out.printf("%.2f%n ",add);
                    break;
                
                case 2:
                    sub= num1 - num2;
                    System.out.printf("%.2f%n ",sub);
                    break;
                    
                case 3:
                    mul= num1 * num2;
                    System.out.printf("%.2f%n ",mul);
                    break;
                    
                case 4:
                    div= num1 / num2;
                    System.out.printf("%.2f%n ",div);
                    break; 
                    
                case 0:
                    System.exit(0);
            }
        }
    }   
}
