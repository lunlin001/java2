/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex01numbers;
import java.util.*;
/**
 *
 * @author 1796144
 */
public class Ex01Numbers {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        double[] number= new double[5];
    
        Scanner console= new Scanner(System.in);
        
        System.out.print("Please enter minimum:");
	int min= console.nextInt();
	System.out.print("Please enter maximum: ");
	int max= console.nextInt();
        
        if(max < min){
             System.out.println("Please make sure the maximum is not smaller than minumum");
             return;
        }
        for(int i= 0 ; i<number.length ; i++){
            double val= Math.random()*(max - min) + min;
            number[i] = val;      
        }
        for(int i= 0 ; i<number.length ; i++){
            double val = number[i];
            System.out.printf("%s%.2f", (i==0 ? "" : ", ") , val);
        }
        System.out.println();
    }  
}
