/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06people;

import java.util.ArrayList;

class Person{
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    
    void print(){
        System.out.printf("Name: %s, Age: %d\n", name, age);
    }
    
}

public class Day06People {

    public static void main(String[] args) {
        ArrayList<Person> people = new ArrayList<>();
        people.add(new Person("Michael",30));
        people.add(new Person("Jordan",50));
        people.add( new Person("Lin",34));
        
        for(int i = 0; i < people.size(); i++){
            Person p = people.get(i);
            p.print();
        }
      
       int oldest= people.get(0).age;
       int index = 0;
       for(int i =0; i < people.size(); i++){
           if(oldest < people.get(i).age){
              oldest= people.get(i).age;
              index = i;
           }
       }
       System.out.printf("The oldest person is %s, age %d\n" , people.get(index).name , oldest);
    }
    
}
