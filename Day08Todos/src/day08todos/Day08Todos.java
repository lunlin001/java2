/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day08todos;

import java.util.Scanner;

class Todo {

    private String task;
    private String dueDate;
    private int hoursOfWork;

    public Todo(String task, String dueDate, int hoursOfWork) throws Exception {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
    }

    public void print() {
        System.out.println("Todo: " + task + " " + dueDate + " " + "will take " + hoursOfWork + "(s) of work");
    }

    /**
     * @return the task
     */
    public String getTask() {
        return task;
    }

    /**
     * @param task the task to set
     */
    public void setTask(String task) throws Exception {
        if (task.length() < 2 || task.length() > 50) {
            throw new Exception("Task must be 2-50 characters long");
        }
        this.task = task;
    }

    /**
     * @return the dueDate
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate the dueDate to set
     */
    public void setDueDate(String dueDate) throws Exception {
        if (dueDate.length() < 2 || dueDate.length() > 20) {
            throw new Exception("Due date must be 2-20 characters long");
        }
        this.dueDate = dueDate;
    }

    /**
     * @return the hoursOfWork
     */
    public int getHoursOfWork() {
        return hoursOfWork;
    }

    /**
     * @param hoursOfWork the hoursOfWork to set
     */
    public void setHoursOfWork(int hoursOfWork) throws Exception {
        if (hoursOfWork < 0) {
            throw new Exception("Hours must be 0 or greater number!");
        }
        this.hoursOfWork = hoursOfWork;
    }
}

public class Day08Todos {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print("Enter name: ");
            String name = input.nextLine();
            System.out.print("Enter due date: ");
            String dDate = input.nextLine();
            System.out.print("hours of work (integer): ");
            int hour = input.nextInt();
            input.nextLine();
            
            Todo t = new Todo(name, dDate, hour);
            t.print();
        }catch(Exception ex){
            System.out.println("Error while creating Todo: " + ex.getMessage());
        }

    }

}
