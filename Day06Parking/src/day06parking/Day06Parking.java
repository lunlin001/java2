/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06parking;

class Car{
    
    String color;
    double engineSizeLitres;
    String makeModel;
    int yearOfYear;

    public Car(String color, double engineSizeLitres, String makeModel, int yearOfYear) {
        this.color = color;
        this.engineSizeLitres = engineSizeLitres;
        this.makeModel = makeModel;
        this.yearOfYear = yearOfYear;
    }
    void print(){
        System.out.printf("Car: %s, %.2f, %s , %d\n", 
                           color, engineSizeLitres, makeModel, yearOfYear);
    }
   
}
        
public class Day06Parking {

    public static void main(String[] args) {
  
        Car c1 = new Car("red", 3.0, "Audi A8", 2017);
        Car c2 = new Car("yellow", 4.0, "BMW X6", 2015);
        c1.print();
        c2.print();
        
    }
    
    public static void example(){
        
    }
    
}
