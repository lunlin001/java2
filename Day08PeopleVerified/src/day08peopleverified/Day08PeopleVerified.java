/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day08peopleverified;

class Person{

    public Person(String name, int age) throws Exception{
        setName(name);
        setAge(age);
    }
    
    private String name;
    private int age;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) throws Exception{
        if(name.length()<2){
            throw new Exception("Name must be least 2 characters long");
        }
        this.name = name;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) throws Exception{
        if(age < 0 || age >150 ){
            throw new Exception ("Age must be between 0 and 150");
        }
        this.age = age;
    }
}

public class Day08PeopleVerified {

    public static void main(String[] args) {
        try{
            Person p = new Person("L", -1 );
            p.setName("M");
            p.setAge(151);
        }catch(Exception ex){
            System.out.println("Error occurred(2): " + ex.getMessage());
        }
    }
    
}
