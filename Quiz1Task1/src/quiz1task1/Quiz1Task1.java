/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1task1;

/**
 *
 * @author 1796144
 */
public class Quiz1Task1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] array = new int[10];

        for (int i = 0; i < 10; i++) {
            int num = (int) (Math.random() * 51 - 25);
            array[i] += num;
            System.out.println(array[i]);
        }
        
        for(int i = 0; i<10; i++){
            if( i%2 == 0 ){
                System.out.println(array[i]);
            }else if(i%3 == 0){
                System.out.println(array[i]);
            }else{
                break;
            }
        }

        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        System.out.printf("Sum is: %d\n", sum);
        System.out.printf("Average is %d\n", sum / array.length);

        int smallest = array[0];
        int secondSmallest = array[0];
        for (int i = 0; i < array.length; i++) {  
            for (int j = 0; j < array.length; j++){
                if (array[i] < smallest) {
                    smallest = array[i];
                    if (array[j] < secondSmallest){
                        secondSmallest = array[j];
                    }
                }
            }
        }
        System.out.printf("Smallest: %d,secondSmallest : %d\n ",smallest,secondSmallest);  
    }
    
}
