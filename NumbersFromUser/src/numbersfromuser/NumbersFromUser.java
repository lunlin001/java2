/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numbersfromuser;

import java.util.Scanner;

/**
 *
 * @author 1796144
 */
public class NumbersFromUser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        int count = 0;
        double sum= 0;
        while(true){
            System.out.print("Enter a  number , 0 to end: ");
            double num= input.nextDouble();
            
            if(num == 0.0){
                break;
            }
            count++;
            sum+= num;
        }
        
        System.out.printf("Sum of all numbers is: %.2f, average is %.2f\n",sum, sum/count);
    }
    
}
