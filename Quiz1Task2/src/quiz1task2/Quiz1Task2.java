/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1task2;

import java.util.Scanner;

/**
 *
 * @author 1796144
 */
public class Quiz1Task2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[] cityArray = new String[5];
        double[] popArray = new double[5];
         for (int i = 0; i < 5; i++) {
            System.out.printf("Enter name of city : ");
            String city = input.nextLine();
            System.out.printf("Enter population : ");
            double pop = input.nextDouble();
            input.nextLine(); 
            cityArray[i] = city;
            popArray[i] = pop;
            
        }
         
        System.out.print("Enter search string: ");
        String search = input.nextLine();        
        for (int i=0; i<5; i++) {
            if (cityArray[i].contains(search)) {
                System.out.printf("Matching city: %s, %.3f\n" , cityArray[i], popArray[i]);
            }
        }
        
        double largest= popArray[0];
        for (int i = 0; i < popArray.length; i++) {            
            //max = Math.max(numList[i], max);
            largest = (popArray[i] > largest ? popArray[i] : largest);
        }
        System.out.printf("the city of the largest population : %.2f", largest);
        
    }
    
}
