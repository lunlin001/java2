/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10sureinput;

import java.util.Scanner;

public class Day10SureInput {

    static Scanner input = new Scanner(System.in);

    public static int inputInt() {
        while (true) {
            try {
                int val = input.nextInt();
                return val;
            } catch (java.util.InputMismatchException ex) {
                input.nextLine();
                System.out.println("Errors , enter a valid integer!");
                System.out.print("Enter a integer: ");
            }  
        }
    }

    public static void main(String[] args) {
            System.out.print("Enter a integer: ");
            int val = inputInt();
            System.out.println("You enter the number: " + val);
    }

}
