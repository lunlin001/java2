/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex02arraylist;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author 1796144
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Integer> nameList = new ArrayList<>();
        System.out.print("how many numbers he/she wants to generate: ");
        int count= input.nextInt();
        
        for(int i =0; i< count; i++){
            int value= (int)(Math.random()*201 - 100);
            nameList.add(value);
        }   
            
         for(int n : nameList){
            if(n<=0){
                System.out.println(" " + n);
            }
         }       
    }
}
