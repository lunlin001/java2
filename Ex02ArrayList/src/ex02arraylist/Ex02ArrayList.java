/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex02arraylist;

import java.util.ArrayList;

/**
 *
 * @author 1796144
 */
public class Ex02ArrayList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<String> nameList = new ArrayList<>();
        System.out.println("Size: " + nameList.size());
        nameList.add("Jerry");
        nameList.add("Maria");
        nameList.add("Laura");
        nameList.add("Terry");
        for(int i = 0; i<nameList.size();i++){
            String n = nameList.get(i);
            System.out.println("Name: " + n);
        }
        System.out.println("Size: " + nameList.size());
        nameList.remove(3);
        System.out.println("Size: " + nameList.size());
        for(String n : nameList){
            System.out.println("Name is: " + n);
        }
    }
    
}
