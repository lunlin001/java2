/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day05grade;

import java.util.Scanner;

/**
 *
 * @author 1796144
 */
public class Day05Grade {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        
        int count = 0;
        double sum = 0.0;
        while (true) {
            System.out.print("Enter literal grade (A, A-, ...): ");
            String gradeStr = input.nextLine().toUpperCase();
            double grade = 0;
            if(gradeStr.equals("")){
                break;
            }
            switch (gradeStr) {
                case "A":
                    grade = 4.0;
                    break;
                case "A-":
                    grade = 3.7;
                    break;
                case "B+":
                    grade = 3.3;
                    break;
                case "B":
                    grade = 3.0;
                    break;
                case "B-":
                    grade = 2.7;
                    break;
                case "C+":
                    grade = 2.3;
                    break;
                case "C":
                    grade = 2.0;
                    break;
                case "D":
                    grade = 1.0;
                    break;
                case "F":
                    grade = 0.0;
                    break;
                default:
                    System.out.println("Invalid grade");
                    System.exit(1);
            }
            System.out.printf("Numerical grade is %.2f\n", grade);
            sum+=grade;
            count++;
        }
        System.out.printf("The average gradeis %.2f\n", sum / count);
    }
}
