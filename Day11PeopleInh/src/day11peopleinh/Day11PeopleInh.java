/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11peopleinh;

import static java.lang.String.format;
import java.util.ArrayList;

class Person extends Object{

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    
    public String toString(){
        return String.format("Person: %s, %d", name, age);  
    }
    
    String name;
    int age;
}

class Student extends Person{
    Student(String name, int age, String program, double gpa){
        super(name, age);
        this.program = program;
        this.gpa = gpa;
    }
    
    public String toString(){
        return String.format("Person: %s, %d, %s, %.2f", name, age, program, gpa);  
    }
    
    String program;
    double gpa;
}

class Teacher extends Person{
    Teacher(String name, int age, String discipline, int yearOfEx){
        super(name,age);
        this.discipline = discipline;
        this.yearOfEx = yearOfEx;
    }
    
    public String toString(){
        return String.format("Person: %s, %d, %s, %d", name, age, discipline, yearOfEx);  
    }
    
    String discipline;
    int yearOfEx;
}

public class Day11PeopleInh {

    static ArrayList<Person> peopleList= new ArrayList<>();
    
    public static void main(String[] args) {
        peopleList.add(new Person("Jerry", 33));
        peopleList.add(new Teacher("Maria", 47, "Math", 10));
        peopleList.add(new Student("Larry", 27, "Java", 3.74));
        
        for(Person p : peopleList){
            System.out.println(p.toString());
        }
    }
    
}
