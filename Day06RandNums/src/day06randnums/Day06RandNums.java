/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06randnums;

import java.util.ArrayList;

class RandomStore{
    int nextInt(int minIncl,int maxExcl) {
        int range = maxExcl - minIncl;
        int val = (int)(Math.random()* range + minIncl);
        intHistory.add(val);
        return val;
    }
    
    void printHistory() {
        for(int n : intHistory ){
            System.out.print(n + " ");
        }
        System.out.println("");
    }
    
    ArrayList<Integer> intHistory = new ArrayList<>();
}

public class Day06RandNums {

    public static void main(String[] args) {
        RandomStore rs = new RandomStore();
	int v1 = rs.nextInt(1, 10);
	int v2 = rs.nextInt(-100, -10);
	int v3 = rs.nextInt(-20,21);
	rs.printHistory();
    }
    
}
