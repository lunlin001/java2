/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10airporttravel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class ParameterInvalidException extends Exception {

    public ParameterInvalidException(String message) {
        super(message);
    }
}

class Airport {

    public Airport(String code, String city, double latitude, double longitude) {
        this.code = code;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    private String code;
    private String city;
    private double latitude;
    private double longitude;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) throws Exception {
        if (!code.matches("[A-Z]{3}")) {
            throw new Exception("Errors");
        }
        this.code = code;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) throws Exception {
        if ((city == null) || (city.length() == 0)) {
            throw new Exception("Errors");
        }
        this.city = city;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) throws ParameterInvalidException {
        if (latitude > 90 || latitude < -90) {
            throw new ParameterInvalidException("Errors");
        }
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) throws ParameterInvalidException {
        if (longitude > 90 || longitude < -90) {
            throw new ParameterInvalidException("Errors");
        }
        this.longitude = longitude;
    }
}

public class Day10AirportTravel {

    static Scanner input = new Scanner(System.in);
    static ArrayList<Airport> airportList = new ArrayList<>();

    public static int getUserMenuChoice() {
        while (true) {
            System.out.print("Please make a choice [0-4]:\n"
                    + "1. Show all airports (codes and city names)\n"
                    + "2. Find distance between two airports by codes\n"
                    + "3. Find the 2 airports nearest to an airport given and display the distance.\n"
                    + "4. Add a new airport to the list.\n"
                    + "0. Exit\n"
                    + "Your choice is: ");
            int choice = input.nextInt();
            input.nextLine();
            return choice;
        }
    }

    public static void listAll() {
        try (Scanner fileInput = new Scanner(new File("airports.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String data[] = line.split(";");
                if(line.length() != 4){
                    System.out.println("");
                }
                String code = data[0];
                String city = data[1];
                double latitude = fileInput.nextDouble();
                double longitude = fileInput.nextDouble();
                fileInput.nextLine();
                Airport airport = new Airport(code, city, latitude, longitude);
                airportList.add(airport);
            }
        } catch (IOException ex) {
            System.out.println("File reading error: " + ex.getMessage());
        }
        //
        for (Airport a : airportList) {
            System.out.println("  " + a.getCode() + a.getCity() + a.getLatitude() + a.getLongitude());
        }
    }

    public static void main(String[] args) {
        while (true) {
            int choice = getUserMenuChoice();
            if (choice < 0 || choice > 4) {
                System.out.println("Invalid choice, try again.");
                continue;
            }
            if (choice == 0) {
                System.out.println("Exiting. Good bye!");
                return;
            }
            switch (choice) {
                case 1:
                    listAll();
                    break;
                case 2:

                    break;
                case 3:

                    break;
                case 4:

                    break;
                default:
                    System.out.println("Internal error, unable to handle the choice");
                    System.exit(1);
            }
            System.out.println("");
        }
    }

}
