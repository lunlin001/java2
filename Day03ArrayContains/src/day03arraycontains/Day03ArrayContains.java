/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03arraycontains;

/**
 *
 * @author 1796144
 */
public class Day03ArrayContains {

    public static void printDups(int[] a1, int a2[]) { 
        for(int i =0; i < a1.length; i++){
            int value= a1[i];
            for(int j =0; j < a2.length; j++){
                if(value == a2[j]){
                    System.out.println(value);
                    break;
                }
            }
        }
    }
    
    public static int[] removeDups(int[] a1, int[] a2) { 
        int nonDupCount = a1.length;
        for(int i = 0; i< a1.length; i++){
            int value = a1[i];
            for(int j =0; j < a2.length; j++){
                if(value == a2[j]){
                    nonDupCount--;
                    break;
                }
            }
        }
        System.out.println("Non dup count: " + nonDupCount);
        
        int[] result = new int[nonDupCount];
        int resIdx= 0;
        
        for(int i = 0; i< a1.length; i++){
            int value= a1[i];
            boolean isDup= false;
            for(int j =0; j < a2.length; j++){
                if(value == a2[j]){
                    isDup= true;
                    break;
                }
            }
            
            if(!isDup){
                result[resIdx]= value;
                resIdx++;
            }
        }
        return result;    
    }
    
    public int[] concatenate(int[] a1, int[] a2) { 
        int[] result= new int[a1.length + a2.length];
        for(int i = 0; i< a1.length; i++){
            result[i]= a1[i];
        }
        for(int i =0; i < a2.length; i++){
            result[i + a1.length]= a2[i];
        }
        
        return result; 
    }
    
    public static void main(String[] args) {
        int [] a1= {1,4,9,7,2,8,7};
        int [] a2= {1,2,3,4};
        printDups(a1, a2);
        int [] res = removeDups(a1, a2);
    }
    
}
