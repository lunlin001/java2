/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07strings;

class Data{
    public int v;
}

public class Day07PassData {
    
    static Data method(int i, Data d){
        d.v =  i+ 3;
        System.out.println("d=" + d.v);
        
        d= new Data();
        System.out.println("d=" + d.v);
        
        i += 2;
        d.v = i;
        System.out.println("d=" + d.v);
        
        return d;
        
    }

    public static void main(String[] args) {
        Data x = new Data();
        x.v = 7;
        Data y = method(1, x);
        System.out.println("x=" + x.v + ", y=" + y.v);
    }
    
}
