/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06enterpeople;

import java.util.ArrayList;
import java.util.Scanner;

class Person {

    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void print() {
        System.out.printf("Name: %s, Age: %d\n", name, age);
    }

}

public class Day06EnterPeople {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        ArrayList<Person> peopleList = new ArrayList<>();

        while (true) {
            System.out.print("Enter name: ");
            String name = input.nextLine();
            if (name.equals("")) {
                break;
            }
            System.out.print("Enter age: ");
            int age = input.nextInt();
            input.nextLine();
            peopleList.add(new Person(name, age));
        }

        String letter = "";

        System.out.print("Enter one character to search for: ");
        letter = input.nextLine();

        int sum = 0;
        int count = 0;
        for (int i = 0; i < peopleList.size(); i++) {
            sum += peopleList.get(i).age;
            count++;
        }
        System.out.printf("Average age is %.2f\n", (double) (sum / count));

        int youngest = peopleList.get(0).age;
        int index = 0;
        for (int i = 0; i < peopleList.size(); i++) {
            if (youngest > peopleList.get(i).age) {
                youngest = peopleList.get(i).age;
                index = i;
            }
        }
        System.out.printf("The youngest person is %s, age %d\n", peopleList.get(index).name, youngest);

        int shortIndex= 0;
        for (int i = 0; i < peopleList.size(); i++) {
            Person p = peopleList.get(i);
            Person shortNamePerson = peopleList.get(shortIndex);
            if(shortNamePerson.name.length() > p.name.length()){
                shortIndex = i;
            }
        }
        System.out.printf("Person with shortest name is %s, age %d\n", peopleList.get(shortIndex).name, peopleList.get(shortIndex).age);

        for (int i = 0; i < peopleList.size(); i++) {
            if (peopleList.get(i).name.contains(letter)) {
                System.out.printf("Matching name : %s, age %d\n", peopleList.get(i).name, peopleList.get(i).age);
            }
        }

    }

}
