/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz2passports;

import java.util.ArrayList;
import java.util.Scanner;

class InvalidValueException extends Exception {

    InvalidValueException(String message) {
        super(message);
    }
}

class Passport {

    private String number; // passport number AB123456 format exactly
    private String firstName; // year of birth - between 1900-2050
    private String lastName;
    private String address;
    String city;
    double heightCm;
    double weightKg;
    int yob;

    public Passport(String number, String firstName, String lastName, String address, String city, double heightCm, double weightKg, int yob) {
        this.number = number;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.heightCm = heightCm;
        this.weightKg = weightKg;
        this.yob = yob;
    }

    public void print() {
        System.out.printf("Pserson info: %s, %s, %s, %s ,%s ,%.2f, %.2f ,%d\n",
                number, firstName, lastName, address, city, heightCm, weightKg, yob);
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) throws Exception {
        if (number.charAt(0) <= 'A' || number.charAt(1) >= 'Z') {
            throw new Exception("passport number AB123456 format exactly");
        } else if (number.length() < 0 || number.length() > 8) {
            throw new Exception("passport number AB123456 format exactly");
        }
        this.number = number;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) throws Exception {
        if (firstName.length() < 2) {
            throw new Exception("Letter must be over 2");
        }
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) throws Exception {
        if (lastName.length() < 2) {
            throw new Exception("Letter must be over 2");
        }
        this.lastName = lastName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) throws Exception {
        if (address.length() < 2) {
            throw new Exception("Letter must be over 2");
        }
        this.address = address;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) throws Exception {
        if (city.length() < 2) {
            throw new Exception("Letter must be over 2");
        }
        this.city = city;
    }

    /**
     * @return the heightCm
     */
    public double getHeightCm() {
        return heightCm;
    }

    /**
     * @param heightCm the heightCm to set
     */
    public void setHeightCm(double heightCm) throws Exception {
        if (heightCm < 0 || heightCm > 300) {
            throw new Exception("Height must be in 0--300");
        }
        this.heightCm = heightCm;
    }

    /**
     * @return the weightKg
     */
    public double getWeightKg() {
        return weightKg;
    }

    /**
     * @param weightKg the weightKg to set
     */
    public void setWeightKg(double weightKg) throws Exception {
        if (weightKg < 0 || weightKg > 300) {
            throw new Exception("Weight must be in 0--300");
        }
        this.weightKg = weightKg;
    }

    /**
     * @return the yob
     */
    public int getYob() {
        return yob;
    }

    /**
     * @param yob the yob to set
     */
    public void setYob(int yob) {
        this.yob = yob;
    }
}

public class Quiz2Passports {

    static ArrayList<Passport> passList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);

    public static int getChoice() {
        System.out.print("1. Display all passports data (one per line)\n"
                + "2. Display all passports for people in the same city (ask for city name)\n"
                + "3. Find the tallest person and display their info\n"
                + "4. Find the lightest person and display their info\n"
                + "5. Display all people younger than certain age (ask for max age, not year)\n"
                + "6. Add person/passport to list\n"
                + "0. Exit\n"
                + "Your choice is: ");
        int choice = input.nextInt();
        input.nextLine();
        return choice;
    }

    public static void addPerson() {
        try {
            System.out.println("Adding a Person");
            System.out.print("Enter the number: ");
            String number = input.nextLine();
            System.out.print("Enter the firstname: ");
            String firstName = input.nextLine();
            System.out.print("Enter the lastname: ");
            String lastName = input.nextLine();
            System.out.print("Enter the address: ");
            String address = input.nextLine();
            System.out.print("Enter the city: ");
            String city = input.nextLine();
            System.out.print("Enter the height: ");
            double heightCm = input.nextDouble();
            System.out.print("Enter the weight: ");
            double weightKg = input.nextDouble();
            System.out.print("Enter year of birth: ");
            int yob = input.nextInt();
            input.nextLine();
            Passport passport = new Passport(number, firstName, lastName, address, city, heightCm, weightKg, yob);
            passList.add(passport);
            System.out.println("You've created a person:");
        } catch (Exception ex) {
            System.out.println("Error creating person: " + ex.getMessage());
        }
    }

    public static void printList(ArrayList<Passport> list) {
        for (Passport p : list) {
            p.print();
        }
    }

    public static void findCity() {
        String city = "";

        System.out.print("Enter the city to search: ");
        city = input.nextLine();
          
        for(Passport p : passList){
            if(p.city.contains(city)){
                 p.print();
            }
        }
        System.out.println("");
    }

    public static void tallest() {
        int tallestIndex = 0;
        for (int i = 0; i < passList.size(); i++) {
            Passport p = passList.get(i);
            if (passList.get(tallestIndex).heightCm < p.heightCm) {
                tallestIndex = i;
            }
        }
        System.out.print("Tallest person is: ");
        Passport tallest = passList.get(tallestIndex);
        tallest.print();
    }

    public static void lightest() {
        int lightestIndex = 0;
        for (int i = 0; i < passList.size(); i++) {
            Passport p = passList.get(i);
            if (passList.get(lightestIndex).weightKg > p.weightKg) {
                lightestIndex = i;
            }
        }
        System.out.print("Lightest person is: ");
        Passport lightest = passList.get(lightestIndex);
        lightest.print();
    }

    public static void younerAge(){
        final int year = 2017;
        int maxAge = 0;
        System.out.print("Enter the max age: ");
        maxAge = input.nextInt();
        for (Passport p : passList) {
            if (maxAge > (year - (p.yob)) ) {
                p.print();
            }
        }
        System.out.println("");
    }
    public static void main(String[] args) {
        while (true) {
            int choice = getChoice();
            if (choice < 0 || choice > 6) {
                System.out.println("Invalid choice, try again.");
                continue;
            }
            if (choice == 0) {
                System.out.println("Exiting. Good bye!");
                return;
            }
            switch (choice) {
                case 1:
                    printList(passList);
                    break;
                case 2:
                    findCity();
                    break;
                case 3:
                    tallest();
                    break;
                case 4:
                    lightest();
                    break;
                case 5:
                    younerAge();
                    break;
                case 6:
                    addPerson();
                    break;
                default:
                    System.out.println("Internal error, unable to handle the choice");
                    System.exit(1);
            }
            System.out.println("");
        }
    }

}
